<?php
    header('Content-type: application/json');

    function foiMordido($vez) {
        if($vez % 2) {
            return true;
        }
        else {
            return false;
        }
    }

    $vezes = rand(1, 10);
    $retorno = array();

    for($i = 1; $i <= $vezes; $i++) {
        $testaMordida = foiMordido($i);
        $texto = $testaMordida ? "Joãozinho mordeu o  seu dedo!" : "Joãozinho NAO mordeu o seu dedo!";
        $arrayAdd = ["vez" => $i, "resultado" => $testaMordida, "mensagem" => $texto];
        array_push($retorno, $arrayAdd);
    }

    echo json_encode($retorno);
?>