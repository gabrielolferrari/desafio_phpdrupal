<?php
    header('Content-type: application/json');

    function cadastrar($nomeArquivo, $novoCadastro){
        $convert = json_encode($novoCadastro);
        
        file_put_contents($nomeArquivo, $convert, FILE_APPEND);
        file_put_contents($nomeArquivo, "\n", FILE_APPEND);
    }

    function verificarJaCadastrado($dadosSalvos, $email, $login){
        $mensagens = array();

        foreach($dadosSalvos as $dado){
            $info = json_decode($dado, true);

            if($info["email"] == $email || $info["login"] == $login)
            {
                if($info["email"] == $email){
                    array_push($mensagens, "E-mail já cadastrado");
                }
                
                if($info["login"] == $login){
                    array_push($mensagens, "Login já cadastrado");
                }

                return [
                    "resultado" => false,
                    "mensagens" => $mensagens
                ];
            }
        }

        return [
            "resultado" => true,
            "mensagens" => $mensagens
        ];
    }

    function processar(){

        $mensagens = array();

        if($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $nome =& $_POST["nome"];
            $sobrenome =& $_POST["sobrenome"];
            $email =& $_POST["email"];
            $telefone =& $_POST["telefone"];
            $login =& $_POST["login"];
            $senha =& $_POST["senha"];

            $informacoesValidas = true;

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $informacoesValidas = false;
                array_push($mensagens, "E-mail inválido");
            }

            if (preg_match('#^\(\d{2}\) (9|)\d{4}-\d{4}$#', $telefone) < 1) {
                $informacoesValidas = false;
                array_push($mensagens, "Telefone inválido");
            }

            if(!isset($senha) || trim($senha) === ''){
                $informacoesValidas = false;
                array_push($mensagens, "Senha não definida");
            }

            if($informacoesValidas)
            {
                $nomeArquivo = "registros.txt";
                $registros = fopen($nomeArquivo, "rw");
        
                if ($registros) {
                    $cadastrados = array();
        
                    $novoCadastro = [
                        "nome" => $nome,
                        "sobrenome" => $sobrenome,
                        "email" => $email,
                        "telefone" => $telefone,
                        "login" => $login,
                        "senha" => md5($senha)
                    ];
        
                    if(filesize($nomeArquivo) > 0)
                    {
                        $dadosSalvos = explode("\n", fread($registros, filesize($nomeArquivo)));
                        $jaExiste = verificarJaCadastrado($dadosSalvos, $email, $login);
                        if($jaExiste["resultado"])
                        {
                            cadastrar($nomeArquivo, $novoCadastro);
                        }else{
                            return [
                                "resultado" => false,
                                "mensagens" => $jaExiste["mensagens"]
                            ];
                        }
                    } else {
                        cadastrar($nomeArquivo, $novoCadastro);
                    }

                    array_push($mensagens, "Cadastro realizado com sucesso");

                    return [
                        "resultado" => true,
                        "mensagens" => $mensagens
                    ];
                }
            }
            else{
                return [
                    "resultado" => false,
                    "mensagens" => $mensagens
                ];
            }
        }else{
            array_push($mensagens, "Método de acesso inválido.");

            return [
                "resultado" => false,
                "mensagens" => $mensagens
            ];
        }
    }

    echo json_encode(processar());
?>