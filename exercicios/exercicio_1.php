<?php
    header('Content-type: application/json');

    $location = [
        0 => ["pais" => "EUA", "capital" => "​Washington", "prepo" => "dos"],
        1 => ["pais" => "Espanha", "capital" => "Madrid", "prepo" => "da"],
        2 => ["pais" => "Holanda", "capital" => "Amsterdã", "prepo" => "da"],
        3 => ["pais" => "Egito", "capital" => "Cairo", "prepo" => "do"],
        4 => ["pais" => "Brasil", "capital" => "Brasilia", "prepo" => "do"],
    ];

    usort($location, function($a, $b) {
        return $a['capital'] <=> $b['capital'];
    });

    echo json_encode($location);
?>