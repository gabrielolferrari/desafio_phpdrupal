<?php
    header('Content-type: application/json');

    $arquivos = ["music.mp4", "video.mov", "imagem.jpeg"];
    $saida = array();

    foreach($arquivos as $item)
    {
        preg_match('/\.[^\.]+$/i',$item,$split);
        array_push($saida, $split[0]);
    }

    sort($saida);

    echo json_encode($saida);
?>