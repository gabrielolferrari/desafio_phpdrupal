$(document).ready(function() {
    //Chamar o exercício #1
    $.get('exercicios/exercicio_1.php', function(data){
        $.each(data, function(key, value){
            $("#lista_paises").append("<li>A capital " + value.prepo + " <strong>" + value.pais + "</strong> é <strong>" + value.capital + "</strong></li>");
        });
    });

    //Chamar o exercício #2
    $("#provoca_joaozinho").on("click", function(){
        $("#lista_mordidas").empty();
        $.get('exercicios/exercicio_2.php', function(data){
            $.each(data, function(key, value){
                if(value.resultado){
                    $("#lista_mordidas").append("<li><span style='color: green'>" + value.mensagem + "</span></li>");
                }else{
                    $("#lista_mordidas").append("<li><span style='color: red'>" + value.mensagem + "</span></li>");
                }
            });
        });
    });

    //Chamar o exercício #3
    $.get('exercicios/exercicio_3.php', function(data){
        $.each(data, function(key, value){
            $("#lista_extensoes").append("<li><strong>" + value + "</strong></li>");
        });
    });

    //Chamar o exercício #4
    $("#formulario_cadastro").on("click", function(){
        $("#form_sucesso").hide();
        $("#form_erro").hide();

        var cadastro = {
            "nome": $("#form_nome").val(),
            "sobrenome": $("#form_sobrenome").val(),
            "email": $("#form_email").val(),
            "telefone": $("#form_telefone").val(),
            "login": $("#form_login").val(),
            "senha": $("#form_senha").val()
        };

        $.post('exercicios/exercicio_4.php', cadastro, function(data){
            if(data.resultado)
            {
                $("#form_sucesso").show();
            }else{
                $("#form_erro").empty().show();
                $("#form_erro").append("<p>Ops! Verifique:</p>");
                $.each(data.mensagens, function(key, value){
                    $("#form_erro").append("<p>- " + value + "</p>");
                });
            }
        });
    });

    var maskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    options = {
        onKeyPress: function(val, e, field, options) {
            field.mask(maskBehavior.apply({}, arguments), options);
        }
    };
    
    $('#form_telefone').mask(maskBehavior, options);
});